#include "stdafx.h"
#include "stdio.h"
#undef __analysis_assume
#include <CodeAnalysis\SourceAnnotations.h>

//FIXED: changed the buf_size to allow copy_data
#define BUF_SIZE 200
#define STR_SIZE 200

void zeroing();

_Ret_opt_cap_(size) char *my_alloc(size_t size) {
	char *ch  = (char *)malloc(size+1);
	//FIEXED memory can not be allocated, then we shouldn't do operations on a NULL pointer
	if (ch != NULL){
		*ch = NULL;
	//FIXED: size-1 because otherwise writing outside buffer
		ch[size] = NULL; // null terminate here too, to be safe
		return ch; 
	}
	exit(-1);
}

HRESULT input([SA_Post(Tainted=SA_Yes)] _Out_cap_c_(STR_SIZE) char *buf) {
	//FIXED gets_s, because gets is not safe
	return (gets_s(buf, 1) != NULL)?SEVERITY_SUCCESS:SEVERITY_ERROR;
}

_Ret_cap_c_(STR_SIZE) [returnvalue:SA_Post(Tainted=SA_Yes)] char *do_read() {
	char *buf = my_alloc(STR_SIZE);
	// FIXED for pointer values you should use %p and not the usnigned hexidecimal %x
	printf("Allocated a string at %p", buf);
	// FIXED iso ! we changed it for the macro FAILED
	if (FAILED(input(buf))) {
		printf("error!");
		exit(-1);
	}
	// FIXED changed to double = because *buf could be NULL and for that you need double =
	if (*buf == NULL)
		printf("empty string");
	return buf;
}

void copy_data([SA_Pre(Tainted=SA_Yes)][SA_Post(Tainted=SA_Yes)] _In_count_c_(STR_SIZE) char *buf1,
               [SA_Post(Tainted=SA_Yes)] _Out_cap_c_(STR_SIZE) char *buf2) {
	memcpy(buf2,buf1,STR_SIZE); 
	buf2[STR_SIZE-1] = NULL; // null terminate, just in case
}

int execute([SA_Pre(Tainted=SA_No)] _In_ char *buf) {
	return system(buf); // pass buf as command to be executed by the OS
}

void validate([SA_Pre(Tainted=SA_Yes)][SA_Post(Tainted=SA_No)] char *buf) {

    // This is a magical validation method, which turns tainted data
    // into untainted data, for which the code not shown.
    //
    // A real implementation might for example use a whitelist to filter
    // the string.

}

_Check_return_ int test_ready() {
	// code not shown
	return 1;
}

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	char *buf1 = do_read();

	//FIXED: check buffer before reading
	validate(buf1);
	char *buf2 = my_alloc(BUF_SIZE);
	if(!buf2)
	    exit(-1);
	//FIXED: check buffer before reading
	validate(buf2);
	zeroing();
	// FIXED put test_ready in if statement
	if(test_ready()){
	    execute(buf1);
            
            char* buf3 = do_read();
	    //FIXED: check buffer before reading
	    validate(buf3);
	    copy_data(buf3, buf2); 
	    execute(buf2);

	    char *buf4 = do_read();
	    //FIXED: check buffer before reading
	    validate(buf4);
	    execute(buf4);
	}
}

// *****************************************************************

void zero(_Out_cap_(len) int *buf, int len)
{
    int i;
	// FIXED i <= len replaced by i < len
    for(i = 0; i < len; i++)
        buf[i] = 0;
}

void zeroboth(_Out_cap_(len) int *buf, int len, 
              _Out_cap_(len3) int *buf3, int len3)
{
    int *buf2 = buf;
    int len2 = len;
    zero(buf2, len2);
    zero(buf3, len3);
}

void zeroboth2(_Out_cap_(len) int *buf, int len, 
	       _Out_cap_(len3) int *buf3, int len3)
{
	// FIXED swaped len3 and len
	zeroboth(buf, len, buf3, len3);
}

void zeroing()
{
    int elements[200];
    int oelements[100];
    zeroboth2(elements, 200, oelements, 100);
}
